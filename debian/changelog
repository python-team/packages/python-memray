python-memray (1.15.0+dfsg-1) unstable; urgency=medium

  * New upstream version 1.15.0.
  * d/control: update libjs-bootstrap to libjs-bootstrap5 in Depends.

 -- Yogeswaran Umasankar <yogu@debian.org>  Fri, 06 Dec 2024 19:16:12 +0000

python-memray (1.14.0+dfsg-2) unstable; urgency=medium

  * d/control: bump libjs-bootstrap4 to libjs-bootstrap5
    in B-Depends. (Closes: #1088430)

 -- Yogeswaran Umasankar <yogu@debian.org>  Mon, 02 Dec 2024 18:23:58 +0000

python-memray (1.14.0+dfsg-1) unstable; urgency=medium

  * New upstream version 1.14.0.
  * Update patches to fit new upstream version + update email ID in
    patches.
  * d/copyright: update to fit new upstream version.

 -- Yogeswaran Umasankar <yogu@debian.org>  Wed, 11 Sep 2024 21:15:00 +0000

python-memray (1.13.4+dfsg-4) unstable; urgency=medium

  * Skip test_tracking.py failing autopkgtests in armhf.

 -- Yogeswaran Umasankar <yogu@debian.org>  Wed, 14 Aug 2024 15:00:00 +0000

python-memray (1.13.4+dfsg-3) unstable; urgency=medium

  * Update the list of tests to skip in d/rules and d/tests/runtests.

 -- Yogeswaran Umasankar <yogu@debian.org>  Wed, 14 Aug 2024 04:00:00 +0000

python-memray (1.13.4+dfsg-2) unstable; urgency=medium

  * Add d/tests to fix test failures in various ARCH.
  * d/rules: define and skip failed tests in each ARCH.
  * d/control: remove testsuite.
  * d/copyright: update to GPL-2+, GPL-3+ and LGPL-2+.

 -- Yogeswaran Umasankar <yogu@debian.org>  Sun, 11 Aug 2024 01:00:00 +0000

python-memray (1.13.4+dfsg-1) unstable; urgency=medium

  * New upstream version 1.13.4.
  * Disable tests in ARCH with timeout and segfault.

 -- Yogeswaran Umasankar <yogu@debian.org>  Mon, 22 Jul 2024 01:40:00 +0000

python-memray (1.13.3+dfsg-2) unstable; urgency=medium

  * d/control: replace python3.*-dev with python3-all-dev + added
    pybuild-plugin-pyproject in Build-Depends. (Closes: #1076202)
  * d/rules: updated override_dh_install for pybuild-plugin-pyproject.

 -- Yogeswaran Umasankar <yogu@debian.org>  Fri, 12 Jul 2024 15:30:00 +0000

python-memray (1.13.3+dfsg-1) unstable; urgency=medium

  * New upstream version 1.13.3.
  * Rename d/python-memray-doc.doc to d/python-memray-doc.docs.

 -- Yogeswaran Umasankar <yogu@debian.org>  Mon, 08 Jul 2024 13:30:00 +0000

python-memray (1.13.2+dfsg-1) unstable; urgency=medium

  * Updated to new upstream version 1.13.2.
  * d/control: added python3.11-dev to Build Depends +
    updated Uploaders.
  * d/copyright: updated 2019-2020 Red Hat, Inc.
    copyright file name + updated debian/* changelog.
  * d/rules: remove duplicate files in docs and create
    symbolic links.

 -- Yogeswaran Umasankar <yogu@debian.org>  Fri, 28 Jun 2024 09:00:00 +0000

python-memray (1.13.0+dfsg-1) unstable; urgency=medium

  * Updated to new upstream version 1.13.0.
  * Updated old patches to fit new upstream version.
  * Added new patches to fix tests in Python 3.12 and
    to fix the docs.
  * d/control: Added libunwind-dev to B-Depends.
  * d/rules: Revised pybuild test args to fir new
    upstream version + enabled memray binary.
  * Added man pages for memray binary.

 -- Yogeswaran Umasankar <kd8mbd@gmail.com>  Sat, 22 Jun 2024 00:05:00 +0000

python-memray (1.11.0+dfsg-1) unstable; urgency=medium

  * Initial release. (Closes: #1067871)

 -- Yogeswaran Umasankar <kd8mbd@gmail.com>  Mon, 27 May 2024 16:50:00 +0000
